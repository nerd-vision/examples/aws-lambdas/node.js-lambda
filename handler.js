'use strict';

const {nerdvision} = require("@nerdvision/agent");

module.exports.handler = async (event) => {
    await nerdvision.initLambda({
        api_key: process.env.NV_API_KEY
    })
    const name = event.name;
    const resp = {
        name: name
    }
    return resp
};
