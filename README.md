# Requirements

 - node 12

## Configuration

 - install and configure the [AWS cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) tools to deploy this example.
 - install and configure the [serverless cli](https://www.serverless.com/framework/docs/getting-started/)
 - set the nerd.vision API key in the serverless.yml file
 
## Build

1. run `serverless deploy`

## Invoke

To invoke the function you can use this command:

```bash
 serverless invoke -d '{"name":"hello"}' -f example
```
